//
//  AppDelegate.h
//  SpheroDrive
//
//  Created by Victor Rosas on 2/8/16.
//  Copyright © 2016 Victor Rosas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

